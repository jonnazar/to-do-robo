# Projects

## RoboDog gait and planning

* approach: model-based with a movement planning module (continuous action space)
- [ ] have a look at: mu-zero with a VQ layer, efficient-zero

## Suction Cup control for a RoboHand

- [ ] ddpq with a one-step local model lookahead and appxrimate terminal q-factor
- [ ] find some literature
